package com.dietsodasoftware;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.inject.Inject;
import lombok.Getter;

@Getter
public class Thing {
  private final String thing;

  @Inject
  public Thing(String thing) {
    this.thing = checkNotNull(thing);
  }
}

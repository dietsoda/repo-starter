This is to make all your dreams come true; it helps turn your project into a turnkey GitLab/Bintray 
CI-enabled demigod.

# Welcome Back
You know why you're here.  You're looking for a refresher.  If you're new here, skip this.

* Add Bintray credential env variables to group or project: `BINTRAY_DIETSODA_REPO_USER` and `BINTRAY_DIETSODA_REPO_PASS`
  * defined for scm server in `.m2/settings.xml` used by scm plugin
* Add GitLab SSH key
  * Create SSH key: `ssh-keygen -t rsa -b 2048`.  Don't use a passphrase
  * Upload public key to your personal profile: `id_rsa.pub`
  * Upload private key env variable to the group or project: `SSH_PRIVATE_KEY`
* [This single commit](https://gitlab.com/dietsoda/yail/commit/84cea2ab3c464488b4a9adb6241b3a3fe4bc82dc)
adds the source you'll need.  Of course, this of a dated version of this repo but demonstrates how to do it.

# Welcome

It makes a few assumptions.

* A artifact repo server, by default with `id`: `bintray-dietsoda`
* The artifact's server credentials are in settings.xml using the same `id`.
* The project's non-standard `./.m2/settings.xml` file is used in `bintray_release.sh`.
* Local development doesn't need to change: you can set up the server in your `${HOME}/.m2/settings.xml`.
* Your bintray org, repo and package are proeprties in `pom.xml`
* You want `javax.inject` (`Provider` and `@Inject` notably), Guava, Commons lang3, and Lombok.  You're welcome.
* I'm using the Open-Source Bintray, which doesn't allow for `SNAPSHOT` builds. Because it is free.  So
snapshots are now releases.  Whatever.

So let's get started. You'll need to:

* Modify `pom.xml`
* Set Up Git's `user.email` and `user.name` for commits (tag pushes)
* Configure GitLab env variables

# Confession

At the time of writing, tag pushing doesn't work because presumably I need to configure SSH keys.  For GitLab CI.
To push tags to the same project's repo.  Yes; that.

#POM

Set up your project's `pom.xml` by modifying the following:

* Proejct's `artifactId`.  Look for `YOUR-ARTIFACT-ID` and update it.
* Project's `name`. Look for `YOUR-PROJECT-NAME` and update it.
* Project's `url`. Look for `YOUR-PROJECT-URL` and update it.
* A few of the proejct's `properties`.
  * `<bintray.org>YOUR-BINTRAY-ORG</bintray.org>`
  * `<bintray.repo>YOUR-BINTRAY-REPO</bintray.repo>`
  * `<bintray.package>YOUR-BINTRAY-PACKAGE</bintray.package>`

Then add your project's dependencies and dependency versions.  Look for markers in `pom.xml`:

* `<!-- YOUR DEPENDENCIES HERE -->`
* `<!-- YOUR DEPENDENCIES' VERSIONS HERE -->`

Be sure to update your `distributionManagement.repository` settings.  Look for the following marker in `pom.xml`:

* `<!-- YOUR connection, url, developerConnection -->`

# GitLab

In either your GitLab repo or your organization, define your bintray credentials as protected environment variables:

* `BINTRAY_DIETSODA_REPO_USER`: your artifactory username
* `BINTRAY_DIETSODA_REPO_PASS`: your artifactory API key

On GitLab, modify it in the best place for you:

* Project: `https://gitlab.com/<group>/<project repo>/settings/ci_cd`
* Group: `https://gitlab.com/groups/<group>/-/settings/ci_cd`

The server ID needs to match in `pom.xml` and whichever `settings.xml` is effective: on gitlab, in `./.m2` and 
locally in `${HOME}/.m2`.


# Git

Lasly, update your CI's `user.email` and `user.name`.  Look for the following marker in `.gitlab-ci.yml`:

* `YOUR CI COMMIT INFO HERE`


# Background

Here are a bunch of articles across the interwebs that informed this pile bit of work:

* [Some guy making sense ditching the Maven Release Plugin](https://axelfontaine.com/blog/dead-burried.html). `bintray_release.sh` is basically enabling this exactly, using configuration in `pom.xml`, `.m2/settings.xml`, and GitLab configuration for env variables.
* [The same guy's previous post walking through alternatives to the relase plugin, setting up that post](https://axelfontaine.com/blog/final-nail.html)
* [GitLab's recommendation for using non-standard settings.xml for releasing](https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/)

Painful reading and reference:

* [Maven's "CI Friendly" features](https://maven.apache.org/maven-ci-friendly.html)
* [More of that crap if you're still curious](http://blog.kiwitype.com/2017/maven-versioning-in-a-continuous-delivery-pipeline/)
* [Maven Deploy Plugin](https://maven.apache.org/plugins/maven-deploy-plugin/). You will be better served reading about the "CI Friendly" usage above.
* [Maven SCM Plugin](https://maven.apache.org/scm/maven-scm-plugin/usage.html)
* [Maven Versions Plugin](https://www.mojohaus.org/versions-maven-plugin/usage.html)

409 Conflict Confusion:

Encountered for lots of reasons, the `409 Conflict` is a beast.

* [409 Conflicts](https://stackoverflow.com/questions/12734788/maven-deploydeploy-file-fails-409-conflict-yet-artifact-uploads-successfully)
* [409 Conflict Workaround](https://stackoverflow.com/questions/27416218/return-code-is-409-reasonphraseconflict-jcenter) (warning: ghetto)
